# Use powerline
USE_POWERLINE="true"
# Source manjaro-zsh-configuration
if [[ -e /usr/share/zsh/manjaro-zsh-config ]]; then
  source /usr/share/zsh/manjaro-zsh-config
fi
# Use manjaro zsh prompt
if [[ -e /usr/share/zsh/manjaro-zsh-prompt ]]; then
  source /usr/share/zsh/manjaro-zsh-prompt
fi

alias ls='exa -al --color=always --group-directories-first'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'

alias cp="cp -i"
alias df='df -h'
alias free='free -m'
alias np='nano -w PKGBUILD'
alias more=less

alias webstorm-eap=/home/bharatmk/Documents/IDE/WebStorm-EAP/bin/./webstorm.sh
alias studio-can=/home/bharatmk/Documents/IDE/android-studio-can/bin/./studio.sh

export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

# JAVA_HOME=/home/bharatmk/Documents/SDK/jdk-11.0.12
JAVA_HOME=/home/bharatmk/Documents/SDK/jdk1.8.0_301
PATH=$PATH:$HOME/bin:$JAVA_HOME/bin
export JAVA_HOME
export PATH

export PATH="$PATH:/home/bharatmk/Documents/SDK/flutter/bin"
export PATH="$PATH":"$HOME/Documents/SDK/flutter/.pub-cache/bin"

export PATH="$PATH:/$HOME/.cargo/bin"

neofetch
